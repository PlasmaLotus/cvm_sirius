<?php
	require_once("action/CommonAction.php");

	class InfoRequestAction extends CommonAction {
        public $result = null;
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
            if (isset($_SESSION["sessionKey"]) || isset($_POST["sessionKey"])){
				if ($_POST["request"] == "user-info"){
					$data = array("key" => $_SESSION["sessionKey"]);
					$this->result = $this->callAPI("user-info", $data);
				}
				if ($_POST["request"] == "gameState"){
					$data = array("key" => $_SESSION["sessionKey"]);
					$this->result = $this->callAPI("state", $data);
				}
				if ($_POST["request"] == "joinGame"){
					$data = array();
					$data["key"] =$_SESSION["sessionKey"];
					$data["id"] = $_POST["gameid"];

					$this->result = $this->callAPI("enter", $data);
					if ($this->result == "GAME_ENTERED"){
						$_SESSION["gameID"]= $_POST["gameid"];
					}

					$file = "_log.txt";
					$date = new DateTime();
					$content ="JoinGameAttempt: SessionKey: " . $_SESSION["sessionKey"] . " - GameID: ".$_POST["gameid"] . " - Timestamp: ".$date->getTimestamp() . " - Result: " . $this->result. "\n";
					//file_put_contents($file, $content, FILE_APPEND | LOCK_EX);
				}
				if ($_POST["request"] == "lobby"){
					$data = array("key" => $_SESSION["sessionKey"]);
					$this->result = $this->callAPI("list", $data);
				}
				if ($_POST["request"] == "attack"){
					$data = array();
					$data["key"] =$_SESSION["sessionKey"];
					$data["skill-name"] = $_POST["attackName"];
					$this->result = $this->callAPI("action", $data);
				}
				

				if($_POST["request"] == "soundState"){
					$_SESSION["sound"] = $_POST["sound"];
					$_SESSION["music"] = $_POST["music"];
					$this->result = "Sound State Saved";
				}
			}
			else{
				$this->result = "";
			}
			
		}
	}
