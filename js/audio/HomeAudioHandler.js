class HomeAudioHandler{
    constructor(){
        this.sounds = Array();
        this.events = Array();
        this.music = null;
        this.musicEnabled = true;
        this.soundEnabled = true;
    }
    tick(){
        for (let i = this.events.length-1 ; i >= 0; i--){
            let event = this.events[i];

            if(this.soundEnabled){
                let sound = Audio("audio/countdown.mp3");
                if (sound != null){
                    this.sounds[this.sounds.length] = sound;
                }
            }
            this.events.slice(i, 1);
        }

        for (let i = this.sounds.length-1 ; i >= 0; i--){
            let sound = this.sounds[i];
            if (sound.ended){
                this.sounds.slice(i, 1);
            }
        }
    }

    disableMusic(){
        this.musicEnabled = false;
    }
    enableMusic(){
        this.musicEnabled = true;
    }
    disableSFX(){
        this.soundEnabled = false;
    }
    enableSFX(){
        this.musicEnabled = true;
    }
    queueEvent(event){
        this.events[this.events.length] = event;
    }

}