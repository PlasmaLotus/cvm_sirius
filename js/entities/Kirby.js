//let 

class Kirby{
    constructor(username){
        this.username = username;
        this.sprite = new TiledImage("images/KirbyNormal.png", 14, 14, 75);
        this.groundHeight = canevas.height - 100;
        this.x = -10; 
        this.y = this.groundHeight;
        this.state = "Idle";
        this.idleTime = 0;
        this.moveTime = 0;
        this.moveTimeMax = 0;
        this.targetX = 0;
        this.targetY = 0;
        this.startX = 0;
        this.startY = 0;
        this.speedX = 0;
        this.speedY = 0;
        this.initJumpSpeed = -20;
        this.grav = 1;
        this.maxFallingSpeed = 7;
        //Negative speed means going up
    }
    tick(){
        this._aiMovement();
        this.groundHeight = canevas.height - 100;
        switch(this.state){
            case "Idle":{
                this.idleTime += 1;
                if (this.idleTime >= 150){
                    this.idleTime = 7;
                    this.setState("Idle2");
                }
                break;
            }
            case "Idle2":{
                this.idleTime -= 1;
                if (this.idleTime <= 0){
                    this.idleTime = 0;
                    this.setState("Idle");
                }
                break;
            }
            case "Walk":{
                this.moveTime += 1;
                this.x = (this.targetX - this.startX)*this.moveTime/this.moveTimeMax + this.startX;
                this.y = (this.targetY - this.startY)*this.moveTime/this.moveTimeMax + this.startY;
                if (this.targetX >this.startX){
                    if (this.x >this.targetX){
                        this.x = this.targetX;
                    }
                }
                else{
                    if (this.x <this.targetX){
                        this.x = this.targetX;
                    }
                }

                if (this.targetY >this.startY){
                    if (this.y >this.targetY){
                        this.y = this.targetY;
                    }
                }
                else{
                    if (this.y <this.targetY){
                        this.y = this.targetY;
                    }
                }

                if (this.x == this.targetX && this.y == this.targetY){
                    this.setState("Idle");
                }
                break;
            }
            case "Jumping":{
                this.speedY += this.grav;
                this.y += this.speedY;
 
                if (this.speedY > 0){
                    this.setState("AirTumble");
                }
                break;
            }
            case "AirTumble":{
                this.setState("Falling");
                //console.log("Falling");
                break;
            }
            case "Falling":{
                this.speedY += this.grav;
                this.y += this.speedY;
                //console.log("Y: "+this.y+ " - Ground: "+ this.groundHeight);
                if (this.speedY > this.maxFallingSpeed){
                    this.speedY = this.maxFallingSpeed;
                    //console.log("Going to Falling 2 from Falling");
                    this.setState("Falling2");
                }
                //console.log(this.y > this.groundHeight)
                if (this.y > this.groundHeight){
                    this.y = this.groundHeight;
                    //console.log("Going to Landing from Falling");
                    this.setState("Landing");
                }

                break;
            }
            case "Falling2":{
                this.y += this.maxFallingSpeed;
                this.speedY = this.maxFallingSpeed;
                //console.log("Y: "+this.y+ " - Ground: "+ this.groundHeight);
                if (this.y > this.groundHeight){
                    this.y = this.groundHeight;
                    //console.log("Going to Landing from Falling2");
                    this.setState("Landing2");
                }
                break;
            }
            case "Landing":
            case "Landing2":{
                this.speedY = 0;
                //console.log("Landing");
                this.setState("Idle");
            }


            default:break;

        }
    }
    moveTo(x, y, time){
        this.setState("Walk");
        this.moveTime = 0;
        this.moveTimeMax = time;
        this.targetX = x;
        this.targetY = y;
        this.startX = this.x;
        this.startY = this.y;

        if (this.targetX > this.startX){
            this.sprite.flipped = false;
        }
        else{
            this.sprite.flipped = true;
        }
    }
    draw(){
        //context.draw(this.sprite);
        this.sprite.tick(this.x, this.y, ctx);
    }
    jump(){
        this.setState("Jumping");
        //let airSpeed = -10;
        this.speedY = this.initJumpSpeed;
    }
    floatTo(x, y, time){

    }
    _aiMovement(){
        let randChance =Math.floor((Math.random() * 500) + 1);
        let x = Math.floor((Math.random() * canevas.width) + 1) - 10;
        //console.log(randChance)
        if (randChance == 69)//xdee
        {
            //console.log("Random Movement")
            this.moveTo(x, this.groundHeight, 75);
        }

    }
    setState(state){
        this.state = state;

        switch(this.state){
            case "Idle":{
                this.sprite.changeRow(0);
                this.sprite.setLooped(true);
                this.sprite.changeMinMaxInterval(0, 4);
                break;
            }
            case "Idle2":{
                this.sprite.changeRow(1);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 5);
                
                break;
            }
            case "Walk":{
                this.sprite.changeRow(2);
                this.sprite.setLooped(true);
                this.sprite.changeMinMaxInterval(0, 9);
                break;
            }
            case "Run":{
                this.sprite.changeRow(3);
                this.sprite.setLooped(true);
                this.sprite.changeMinMaxInterval(0, 9);
                break;
            }
            case "Jumping":{
                this.sprite.changeRow(4);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 0);
                break;
            }
            case "AirTumble":{
                this.sprite.changeRow(5);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 7);
                
                break;
            }
            case "Falling":{
                this.sprite.changeRow(6);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 0);
                
                break;
            }
            case "Falling2":{
                this.sprite.changeRow(7);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 1);
                
                break;
            }
            case "Floating":{
                break;
            }
            case "Puff":{
                this.sprite.changeRow(5);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 1);
                
                break;
            }
            case "Landing":{
                break;
            }
            case "Landing2":{
                this.sprite.changeRow(8);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 1);
                break;
            }
            
        }
    }
}