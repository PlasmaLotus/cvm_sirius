<?php
    require_once("action/HomeAction.php");

    $action = new HomeAction();
    $action->execute();

	require_once("partial/header.php");
	
?>
<canvas id="canvas" width="10" height="10">Do i put this here?</canvas>
<h3>Welcome, <?=$_SESSION["username"]?> </h3>
<div id="userInfo">Loading User Info</div>
<div class="soundHandling">
    <?php
        if (isset($_SESSION["sound"])){
            ?>
            <div hidden id="soundEnabled"><?= $_SESSION["sound"] ?></div>
            <?php
        }
        if (isset($_SESSION["music"])){
            ?>
            <div hidden id="musicEnabled"><?= $_SESSION["music"] ?></div>
            <?php
        }
    ?>
    
    <div id="musicTooLoud">Mute Music</div>
    <div id="soundTooLoud">Mute SFX</div>
</div>
<template type="userInfoTemp" id="userInfoTemplate">
    <div class="userInfo">
        <div class="InfoUsername"></div>
        <div class="InfoType"></div>
        <div class="InfoLevel"></div>
        <div class="InfoBaseLevelEXP"></div>
        <div class="InfoNextLevelEXP"></div>
        <div class="InfoEXP"></div>
        <div class="InfoVictories"></div>
        <div class="InfoLosses"></div>
        <div class="InfoLastGameState"></div>
        <div class="InfoUnspentPoints"></div>
        <div class="InfoUnspentSkills"></div>
        <div class="InfoWelcomeText"></div>
        <div class="InfoHP"></div>
        <div class="InfoMP"></div>
        <div class="InfoDodgeChance"></div>
        <div class="InfoDmgRed"></div>
    </div>
</template>
<link href="css/lobby.css" rel="stylesheet"/>
<script src="js/audio/audioHandling.js"></script>
<script src="js/audio/HomeAudioHandler.js"></script>
<script src="js/javascript.js"></script>
<script src="js/home.js"></script>
<?php
    require_once("partial/headerLoggedIn.php");
	require_once("partial/footer.php");