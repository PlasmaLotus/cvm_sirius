<?php
    require_once("action/InfoRequestAction.php");

    $action = new InfoRequestAction();
    $action->execute();

	echo json_encode($action->result);