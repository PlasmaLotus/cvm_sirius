<?php
    require_once("action/LobbyAction.php");

    $action = new LobbyAction();
    $action->execute();

	require_once("partial/header.php");
    
?>

<canvas id="canvas" width="42" height="69">Do i put this here?</canvas>
<h3>Welcome to the Lobby, <?=$_SESSION["username"]?></h3>
<div id="gamesContainer" style="display:flex">Loading Lobby</div>
<div class="clear"></div>
<div class="soundHandling">
    <?php
        if (isset($_SESSION["sound"])){
            ?>
            <div hidden id="soundEnabled"><?= $_SESSION["sound"] ?></div>
            <?php
        }
        if (isset($_SESSION["music"])){
            ?>
            <div hidden id="musicEnabled"><?= $_SESSION["music"] ?></div>
            <?php
        }
    ?>
    
    <div id="musicTooLoud">Mute Music</div>
    <div id="soundTooLoud">Mute SFX</div>
</div>


<template type="gameLobbyTemplate" id="gameLobbyTemplateID">
    <div class="gameInfo">
        <div class="gameInfoElement, gameInfoID"></div>
        <div class="gameInfoElement, gameInfoName"></div>
        <div class="gameInfoElement, gameInfoLevel"></div>
        <div class="gameInfoElement, gameInfoCurrentHP"></div>
        <div class="gameInfoElement, gameInfoHP"></div>
        <div class="gameInfoElement, gameInfoUserCount"></div>
        <div class="gameInfoElement, gameInfoNB"></div>
        <div class="gameInfoElement, gameInfoMaxUsers"></div>
        <div class="gameInfoElement, gameInfoType"></div>
        <div class="gameInfoElement, gameInfoWinCount"></div>
        <div class="gameInfoElement, gameInfoLossCount"></div>
    </div>
</template>
<!--
<div id="lobbyEnter" style="visibility:hidden;"> <a href="?gameEntry=true">Get into a Game</a> </div>
<form action="lobby.php" method="post">
    <input class="gameEntry" type="hidden" name="gameEntry" value="Esketit!"></div>
</form> 
-->
<link href="css/lobby.css" rel="stylesheet"/>
<script src="js/audio/LobbyAudioHandler.js"></script>
<script src="js/audio/audioHandling.js"></script>
<script src="js/lobby.js"></script>
<?php
    require_once("partial/headerLoggedIn.php");
	require_once("partial/footer.php");