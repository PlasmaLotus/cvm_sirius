//let 

class You{
    constructor(entity){
        this.image = new Image("images/You.png");
        this.image.src = "images/You.png";
        this.x = 0; 
        this.y = 0;
        this.entity = entity;
    }
    tick(){
        this.x = this.entity.x;
        this.y = this.entity.y - 50;
        //console.log(this.x + "/" + this.y);
    }

    draw(){
        ctx.drawImage(this.image, this.x - 50, this.y- this.image.height /2);
    }

}