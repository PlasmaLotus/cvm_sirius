<?php
    require_once("action/IndexAction.php");

    $action = new IndexAction();
    $action->execute();

	require_once("partial/header.php");
?>

<div class="loginWindow">
<?php
    if($action->loginStatus == "Connected"){
        if(isset($_SESSION["sessionKey"])){
            ?>
            <div class="loginSuccess"> Login Succesful</div>
            <?php
        }
    }
    else{
        if ($action->loginStatus != "Signed Out"){
        ?>
        <div class="loginErrorMessage">
            <div class="loginFailure"> Unable to login...</div>
            <div class="loginFailure"> <?=$action->loginMessage?></div>
        </div>
        <?php            
        }
    }
    
    ?>
<form action="index.php" method="post">
    <div><input class="formHidden" type="hidden" name="login" value="Esketit!"></div>
	<div class="formLabel"><label for="courriel"> Username : </label></div>
	<div class="formInput"><input type="text" name="champUsername" /></div>
	<div class="formSeparator"></div>
	
	<div class="formLabel"><label for="pwd"> Password : </label> </div>
	<div class="formInput"><input type="password" name="champPassword" /></div>
	<div class="formSeparator"></div>
	
	<div class="formLabel">&nbsp;</div>
	<div class="formInput"><button name="loginButton"/>Login</button></div>
    <div class="formSeparator"></div>
</form>
</div>
<?php
    require_once("partial/indexInclude.php");
	require_once("partial/footer.php");