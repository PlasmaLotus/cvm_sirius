<?php
	require_once("action/CommonAction.php");

	class IndexAction extends CommonAction {
		public $loginStatus = "Signed Out";
		public $loginMessage = "Signout Succesful";
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			if(!empty($_POST["login"])){
				$data = [];
				$data["username"] = $_POST["champUsername"];
				$data["pwd"] = $_POST["champPassword"];

				$key = $this->callAPI("signin", $data);
				if (strlen($key) >= 40){
					$_SESSION["sessionKey"] = $key;
					$_SESSION["username"] = $data["username"];
					$this->loginStatus = "Connected";
				}
				else{
					$this->loginStatus = "Not Connected";
					$this->loginMessage = $key;
				}
			}

			if (isset($_SESSION["sessionKey"])){
				header("location:home.php");
				exit();
			}
		}
	}
