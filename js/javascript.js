
let ctx = null;
let canevas = null;
let mousePosX = null;
let mousePosY = null;
window.onload = () =>{
    console.log("test");
	canevas = document.getElementById("canvas");
	ctx = canevas.getContext("2d"); //graphics
    canevas.width = window.innerWidth;
    canevas.height = window.innerHeight;

	/*
    console.log("window.width");
    console.log(window.innerWidth);
    console.log("window.height");
    console.log(window.innerHeight);
    console.log("document.width");
    console.log(document.width);
    console.log("document.width");
    console.log(document.width);
	*/
	
	document.getElementById("container").onmousemove = function (event) {
		mousePosX = event.offsetX;
		mousePosY = event.offsetY;
		//console.log(mousePosX + "," + mousePosY);
	}

	tick();
}

function tick(){
	ctx.clearRect(0,0,canevas.width, canevas.height);
    ctx.fillStyle = "rgba(200, 49, 79, 0.75)";
	ctx.strokeStyle = "rgba(100, 100, 100, 0.75)";
	//ctx.fillRect(10, 10, 50, 100); 
	//ctx.strokeRect(10, 10, 50, 100);

	window.requestAnimationFrame(tick);
}