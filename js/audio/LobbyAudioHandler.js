class LobbyAudioHandler{
    constructor(){
        this.sounds = new Array();
        this.events = new Array();
        this.music = new Audio("audio/Kirby Super Star Ultra Music Great Cave Offensive.mp3");
        this.musicEnabled = true;
        this.soundEnabled = true;
        if (this.music != null){
            this.music.volume = 0.5;
            this.music.loop = true;
            this.music.play();
        }
    }
    tick(){
        for (let i = this.events.length-1 ; i >= 0; i--){
            let event = this.events[i];

            if(this.soundEnabled){
                let sound = null;
                if (event == "LobbyOver"){
                    sound = new Audio("audio/song130.wav")
                }
                else{
                    sound = new Audio("audio/countdown.mp3");
                }
                
                if (sound != null){
                    this.sounds[this.sounds.length] = sound;
                }
            }
            this.events.slice(i, 1);
        }

        for (let i = this.sounds.length-1 ; i >= 0; i--){
            let sound = this.sounds[i];
            if (sound.ended){
                this.sounds.slice(i, 1);
            }
        }
    }

    disableMusic(){
        this.musicEnabled = false;
        if (this.music != null){
            this.music.muted = true;
        }
    }
    enableMusic(){
        this.musicEnabled = true;
        if (this.music != null){
            this.music.muted = false;
        }
    }
    disableSFX(){
        this.soundEnabled = false;
    }
    enableSFX(){
        this.musicEnabled = true;
    }
    queueEvent(event){
        this.events[this.events.length] = event;
    }

}