<?php
	session_start();

	abstract class CommonAction {
		public static $VISIBILITY_MEMBER = 1;
		public static $VISIBILITY_PUBLIC = 0;

		private $visibility = null;

		public function __construct($visibility) {
			$this->visibility = $visibility;
		}

		public function execute() {
			if (isset($_POST["logout"]) || isset($_GET["logout"])) {
				if (isset($_SESSION["sessionKey"])){
					$data = array("key" => $_SESSION["sessionKey"]);
					$result = $this->callAPI("signout", $data);
					if ($result == "USER_SIGNED_OUT"){
						echo $result;
					}
					else{
						echo $result;
					}
					session_unset();
					session_destroy();
					session_start();
				}
			}

			if ($this->visibility > CommonAction::$VISIBILITY_PUBLIC) {
				if (!isset($_SESSION["sessionKey"])) {
					header("location:index.php");
					exit;
				}
			}

			$this->executeAction();
		}

		abstract protected function executeAction();

		/**
		 * data = array('key1' => 'value1', 'key2' => 'value2');
		 */
		protected function callAPI($service, array $data) {
			$apiURL = "https://apps-de-cours.com/web-sirius/server/api/" . $service . ".php";

			$options = array(
				'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
					'method'  => 'POST',
					'content' => http_build_query($data)
				)
			);
			$context  = stream_context_create($options);
			$result = file_get_contents($apiURL, false, $context);
			if (strpos($result, "<br") !== false) {
				var_dump($result);
				exit;
			}
			
			return json_decode($result);
		}


	}
