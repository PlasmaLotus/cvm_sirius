<?php
    require_once("action/GameAction.php");

    $action = new GameAction();
    $action->execute();
    
	require_once("partial/header.php");
	
?>
<canvas id="canvas" width="10" height="10">Do i put this here?</canvas>
<h3><?=$_SESSION["username"]?>, In Game!</h3>

<div class="gameStatus" id="gameStatus">Game Status</div>
<div class="clear"></div>
<div class="gameStatus" id="playerStatus" >Players Status</div>
<div class="clear"></div>
<div id="actions" class="gameStatus" >
    <div id="attack1" class="attackButton">Attack1</div>
    <div id="attack2" class="attackButton">Attack2</div>
    <div id="attack3" class="attackButton">Attack3</div>
    <div id="attack4" class="attackButton">Attack4</div>
</div>
<div class="clear"></div>
<div class="soundHandling">
    <?php
        if (isset($_SESSION["sound"])){
            ?>
            <div hidden id="soundEnabled"><?= $_SESSION["sound"] ?></div>
            <?php
        }
        if (isset($_SESSION["music"])){
            ?>
            <div hidden id="musicEnabled"><?= $_SESSION["music"] ?></div>
            <?php
        }
    ?>
    
    <div id="musicTooLoud">Mute Music</div>
    <div id="soundTooLoud">Mute SFX</div>
</div>

<template type="playerInfoTemp" id="playerInfoTemplate">
    <div class="playerInfo">
        <div class="InfoName"></div>
        <div class="InfoLevel"></div>
        <div class="InfoCurrentHP"></div>
        <div class="InfoHP"></div>
        <div class="InfoMaxHP"></div>
        <div class="InfoCurrentMP"></div>
        <div class="InfoMP"></div>
        <div class="InfoMaxMP"></div>
        <div class="InfoType"></div>
        <div class="InfoVictories"></div>
        <div class="InfoSkills"></div>
    </div>
</template>

<template type="gameInfoTemp" id="GameInfoTemplate">
    <div class="GameInfo">
        <div class="GameInfoName"></div>
        <div class="GameInfoType"></div>
        <div class="GameInfoLevel"></div>
        <div class="GameInfoCurrentHP"></div>
        <div class="GameInfoHP"></div>
        <div class="GameInfoMaxHP"></div>
        <div class="GameInfoAttacked"></div>
        <div class="GameInfoLastTarget"></div>
    </div>
</template>

<template type="SkillInfoTemp" id="SkillInfoTemplate">
    <div class="SkillInfo">
        <div class="SkillName"></div>
        <div class="SkillLevel"></div>
        <div class="SkillDamage"></div>
        <div class="SkillCost"></div>
        <div class="SkillHeal"></div>
        <div class="SkillHealTarget"></div>
    </div>
</template>


<?php
    require_once("partial/gameInclude.php");
    require_once("partial/headerLoggedIn.php");
	require_once("partial/footer.php");