<link href="css/game.css" rel="stylesheet"/>
<script src="js/audio/GameAudioHandler.js"></script>
<script src="js/audio/audioHandling.js"></script>
<script src="js/game.js"></script>
<script src="js/entities/TiledImage.js"></script>
<script src="js/entities/Batafire.js"></script>
<script src="js/entities/Kirby.js"></script>
<script src="js/entities/You.js"></script>
<script src="js/gameAnimation.js"></script>
