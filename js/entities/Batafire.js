
class Batafire{
    constructor(){
        this.sprite = new TiledImage("images/Batafire.png", 14, 14, 75);
        this.x = -50; 
        this.y = -50;
        this.state = "Idle";
        this.idleTime = 0;
        this.moveTime = 0;
        this.moveTimeMax = 0;
        this.targetX = 0;
        this.targetY = 0;
        this.startX = 0;
        this.startY = 0;
        this.speedX = 0;
        this.speedY = 0;
        this.initJumpSpeed = -20;
        this.grav = 1;
        this.maxFallingSpeed = 7;
        this.middleX = 50;
        this.middleY = 50;
        this.nextState = "Idle"
        this.bullets = new Array();
        //this.sprite.scale = 2;
    }
    draw(){
        this.sprite.tick(this.x, this.y, ctx);
        this.bullets.forEach(element => {
            ctx.drawImage(element.sprite, element.x, element.y);
            //console.log("Draw Bullet");
        });
    }
    _aiMovement(){
        let randChance =Math.floor((Math.random() * 1000) + 1);
        let x = Math.floor((Math.random() * canevas.width) + 1);

        if (randChance == 69)//lol
        {
            this.moveTo(Math.floor((Math.random() * canevas.width) + 1), 100, 75);
        }

    }
    tick(){
        for(let i = this.bullets.length -1; i >= 0; i--){
            let bullet = this.bullets[i];
            bullet.tick();
            if (!bullet.alive){
                this.bullets.splice(i, 1);
            }
        }
        this.groundHeight = canevas.height - 100;
        switch(this.state){
            case "Idle":{
                this.idleTime += 1;
                if (this.idleTime >= 150){
                    this.idleTime = 7;
                    this.setState("Idle2");
                }
                break;
            }
            case "Attack1":
            case "Fly":{
                this.moveTime += 1;
                this.x = (this.targetX - this.startX)*this.moveTime/this.moveTimeMax + this.startX;
                this.y = (this.targetY - this.startY)*this.moveTime/this.moveTimeMax + this.startY;
                if (this.targetX >this.startX){
                    if (this.x >this.targetX){
                        this.x = this.targetX;
                    }
                }
                else{
                    if (this.x <this.targetX){
                        this.x = this.targetX;
                    }
                }

                if (this.targetY >this.startY){
                    if (this.y >this.targetY){
                        this.y = this.targetY;
                    }
                }
                else{
                    if (this.y <this.targetY){
                        this.y = this.targetY;
                    }
                }
                if (this.x == this.targetX && this.y == this.targetY){
                    this.setState(this.nextState);
                    this.nextState = "Idle";
                }
                break;
            }
            case "Attack3":{
                let x = Math.floor((Math.random() * canevas.width) + 1) - 10;
                this.moveTo(x, 100, 100);
                audioHandler.queueEvent("BataAtk2");
                break;
            }
            case "Special1":{
                //console.log("in SPecial1");
                this.idleTime += 1;
                if (this.idleTime >= 10){
                    this.idleTime = 0;
                    //console.log("in SPecial2");
                    this.setState("Special3");
                }
                break;
            }
            case "Special2":{
                this.setState("Special3");
                break;
            }
            case "Special3":{
                this.idleTime += 1;
                if (this.idleTime >= 20){
                    this.idleTime = 0;
                    //console.log("in SPecial3");
                    this.setState("Special4");
                }
                break;
            }
            case "Special4":{
                //console.log("in SPecial4");
                this._spawnBullets();
                this.idleTime += 1;
                if (this.idleTime >= 120){
                    this.idleTime = 0;
                    //console.log("in SPecial3");
                    //this.setState("Special3");
                    this.moveTo(Math.floor((Math.random() * canevas.width) + 1), 100, 75);
                }
                break;
            }
            case "Falling":{
                this.speedY += this.grav;
                this.y += this.speedY;
                console.log("Y: "+this.y+ " - Ground: "+ this.groundHeight);
                if (this.speedY > this.maxFallingSpeed){
                    this.speedY = this.maxFallingSpeed;
                    console.log("Going to Falling 2 from Falling");
                    this.setState("Falling2");
                }
                console.log(this.y > this.groundHeight)
                if (this.y > this.groundHeight){
                    this.y = this.groundHeight;
                    console.log("Going to Landing from Falling");
                    this.setState("Landing");
                }

                break;
            }
            case "Falling2":{
                this.y += this.maxFallingSpeed;
                this.speedY = this.maxFallingSpeed;
                console.log("Y: "+this.y+ " - Ground: "+ this.groundHeight);
                if (this.y > this.groundHeight){
                    this.y = this.groundHeight;
                    console.log("Going to Landing from Falling2");
                    this.setState("Landing2");
                }
                break;
            }
            case "Landing":
            case "Landing2":{
                this.speedY = 0;
                break;
            }
            default:break;

        }
    }
    moveTo(x, y, time){
        this.setState("Fly");
        this.moveTime = 0;
        this.moveTimeMax = time;
        this.targetX = x;
        this.targetY = y;
        this.startX = this.x;
        this.startY = this.y;
        this.nextState = "Idle";
        if (this.targetX > this.startX){
            this.sprite.flipped = true;
        }
        else{
            this.sprite.flipped = false;
        }
    }
    moveToSpecial(x, y, time){
        this.setState("Fly");
        this.moveTime = 0;
        this.moveTimeMax = time;
        this.targetX = x;
        this.targetY = y;
        this.startX = this.x;
        this.startY = this.y;
        this.nextState = "Special1";
        if (this.targetX > this.startX){
            this.sprite.flipped = true;
        }
        else{
            this.sprite.flipped = false;
        }
    }
    moveToAttack(x, y, time){
        this.setState("Attack1");
        this.moveTime = 0;
        this.moveTimeMax = time;
        this.targetX = x;
        this.targetY = y;
        this.startX = this.x;
        this.startY = this.y;
        this.nextState = "Attack3";
        if (this.targetX > this.startX){
            this.sprite.flipped = true;
        }
        else{
            this.sprite.flipped = false;
        }
        audioHandler.queueEvent("BataAtk");
    }
    setState(state){
        this.state = state;

        switch(this.state){
            case "Idle":{
                this.sprite.changeRow(0);
                this.sprite.setLooped(true);
                this.sprite.changeMinMaxInterval(0, 9);
                break;
            }
            case "Idle2":{
                this.sprite.changeRow(0);
                this.sprite.setLooped(true);
                this.sprite.changeMinMaxInterval(0, 9);
                
                break;
            }
            case "Fly":{
                this.sprite.changeRow(0);
                this.sprite.setLooped(true);
                this.sprite.changeMinMaxInterval(0, 9);
                break;
            }
            case "Falling":{
                this.sprite.changeRow(9);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 0);
                
                break;
            }
            case "Falling2":{
                this.sprite.changeRow(9);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 0);
                
                break;
            }
            case "Attack1":{
                this.sprite.changeRow(6);
                this.sprite.setLooped(true);
                this.sprite.changeMinMaxInterval(0, 2);
                break;
            }
            case "Attack2":{
                this.sprite.changeRow(7);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 1);
                
                break;
            }
            case "Attack3":{
                this.sprite.changeRow(8);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 3);
                
                break;
            }
            case "Special1":{
                this.sprite.changeRow(1);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 2);
                break;
            }
            case "Special2":{
                this.sprite.changeRow(2);
                this.sprite.setLooped(true);
                this.sprite.changeMinMaxInterval(0, 3);
                
                break;
            }
            case "Special3":{
                this.sprite.changeRow(3);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 8);
                
                break;
            }
            case "Special4":{
                this.sprite.changeRow(4);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 5);
                
                break;
            }
            case "Special5":{
                this.sprite.changeRow(5);
                this.sprite.setLooped(false);
                this.sprite.changeMinMaxInterval(0, 1);
                
                break;
            }
            case "Landing":{
                this.sprite.changeRow(10);
                this.sprite.setLooped(true);
                this.sprite.changeMinMaxInterval(0, 1);
                break;
            }
            case "Landing2":{
                this.sprite.changeRow(11);
                this.sprite.setLooped(true);
                this.sprite.changeMinMaxInterval(0, 3);
                break;
            }
            
        }
    }
    die(){
        this.setState("Falling");
    }
    _spawnBullets(){
        let chance = Math.floor((Math.random() * 15) -5);
        if (chance >= 0){
            for (let i = chance; i >= 0; i--){
                if (this.bullets.length < 50){
                    this.bullets[this.bullets.length] = new Bullet(this.x, this.y);
                }

            }
        }
    }
}

class Bullet{
    constructor(x, y){
        this.sprite = new Image();
        this.sprite.src = ("images/Bullet.png");
        this.x = x; 
        this.y = y;

        this.speedX = Math.floor((Math.random() * 20) - 10);
        this.speedY = 0;
        this.initSpeed = -5;
        this.grav = 1;
        this.maxFallingSpeed = 7;
        this.alive = true;
    }

    tick(){
        this.speedY += this.grav;
        if (this.speedY >= this.maxFallingSpeed){
            this.speedY = this.maxFallingSpeed;
        }
        this.y += this.speedY;
        this.x += this.speedX;
        if (this.x < 0 || this.x > canevas.width){
            this.alive = false;
        }
        if (this.y < 0 || this.y > canevas.height){
            this.alive = false;
        }
        
    }
}