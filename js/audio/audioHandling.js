let musicEnabled = true;
let sfxEnabled = true;
let musicButton = null;
let sfxButton = null;

function loadSoundButtons() {
    musicEnabledDiv = document.getElementById("musicEnabled");
	sfxEnabledDiv = document.getElementById("soundEnabled");
    musicButton = document.getElementById("musicTooLoud");
    sfxButton = document.getElementById("soundTooLoud");
    
    if (musicEnabledDiv != null){
        //console.log(musicEnabledDiv.innerHTML)
        musicEnabled = (musicEnabledDiv.innerHTML == "true");
        //console.log(musicEnabled);
    }
    if (sfxEnabledDiv != null){
        //console.log(musicEnabledDiv.innerHTML)
        sfxEnabled = (sfxEnabledDiv.innerHTML == "true");
        //console.log(sfxEnabled);
    }
    if (musicButton != null){
		musicButton.style.position = "absolute";
		musicButton.style.top = "10px";
		musicButton.style.right = "20px";
		musicButton.onclick = function (event){
			if (musicEnabled){
				musicEnabled = false;
				if (audioHandler != null){
					audioHandler.disableMusic();
				}
				musicButton.innerHTML = "Enable Music";
			}
			else{
				musicEnabled = true;
				if (audioHandler != null){
					audioHandler.enableMusic();
				}
				musicButton.innerHTML = "Mute Music";
            }
            saveSoundState();
        }
    }
    if (!musicEnabled){
        musicEnabled = false;
        //musicButton.click();
        if (audioHandler != null){
            audioHandler.disableMusic();
        }
        musicButton.innerHTML = "Enable Music";
        //console.log("Music isnt Enabled");
    }

    if (sfxButton != null){
		sfxButton.style.position = "absolute";
		sfxButton.style.top = "30px";
		sfxButton.style.right = "20px";
		sfxButton.onclick = function (event){
			if (sfxEnabled){
				sfxEnabled = false;
				if (audioHandler != null){
					audioHandler.disableSFX();
				}
				sfxButton.innerHTML = "Enable SFX";
			}
			else{
				sfxEnabled = true;
				if (audioHandler != null){
					audioHandler.enableSFX();
				}
				sfxButton.innerHTML = "Mute SFX";
            }
            saveSoundState();
        }
    }
    if (!sfxEnabled){
        sfxEnabled = false;
        //sfxButton.click();
        if (audioHandler != null){
            audioHandler.disableSFX();
        }
        sfxButton.innerHTML = "Enable SFX";
        //console.log("Sound isnt Enabled");
    }

}

function saveSoundState(){
    $.ajax({
        type : "POST",
        url : "_infoRequest.php",
        data : {
            request: "soundState",
            music: musicEnabled,
            sound: sfxEnabled
        }
    })
    .done(response => {
		//response = JSON.parse(response);
        //console.log(response);
    });
}