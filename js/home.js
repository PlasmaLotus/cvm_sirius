
let raw = null;
let userInfoContainer = null;
let audioHandler = new HomeAudioHandler();
//let actionsContainer = null;
let MINIMUM_TIME_REQUEST = 10000;
window.onload = () =>{
	
	userInfoContainer = document.getElementById("userInfo");
    loadSoundButtons();
	setTimeout(getUserInfo, 2000);
    tick();
    //updateUserInfo(document.getElementById("rep").innerHTML);
}

function tick(){
	//ctx.clearRect(0,0,canevas.width, canevas.height);
	window.requestAnimationFrame(tick);
}

function getUserInfo(){
    $.ajax({
        type : "POST",
        url : "_infoRequest.php",
        data : {
            request: "user-info",
            sessionKey : "",
        }
    })
    .done(response => {
		response = JSON.parse(response);
        if (typeof response === "object") {
			//console.log("Update Game");
			//console.log("gameFound");
			//raw = response;
			updateUserInfo(response);
			setTimeout(getUserInfo, MINIMUM_TIME_REQUEST);
		}
		else{
			console.log("User Not Found");
            console.log(response);
			//if (response == "USER_NOT_FOUND" || response == "GAME_NOT_FOUND_NONE"){
			window.location.replace("index.php?logout=true");
			//}
			
		}
    });
}

function updateUserInfo(user){
	while(userInfoContainer.firstChild){
		userInfoContainer.removeChild(userInfoContainer.firstChild);
    }
    //console.log(user);
    //user = JSON.parse(user);
    //console.log(user);
	let userInfoTemplate = document.querySelector("#userInfoTemplate").innerHTML;
	let userNode = document.createElement("div");
	userNode.innerHTML = userInfoTemplate;

    userNode.querySelector(".InfoUsername").innerHTML = "UserName: " +user.username;
    userNode.querySelector(".InfoType").innerHTML = "Type: " +user.type;
    userNode.querySelector(".InfoLevel").innerHTML = "Level: " +user.level;
    userNode.querySelector(".InfoBaseLevelEXP").innerHTML = "Base Level XP: " +user.base_level_exp;
    userNode.querySelector(".InfoEXP").innerHTML = "XP: " +user.exp +  "/" + user.next_level_exp;
    //userNode.querySelector(".InfoNextLevelEXP").innerHTML = "Next Level XP: " +user.next_level_exp;
    //userNode.querySelector(".InfoEXP").innerHTML = "XP: " +user.exp;
    userNode.querySelector(".InfoVictories").innerHTML = "Wins: " +user.victories;
    userNode.querySelector(".InfoLosses").innerHTML = "Losses: " +user.loss;
    userNode.querySelector(".InfoLastGameState").innerHTML = "Last Game State: " +user.last_game_state;
    userNode.querySelector(".InfoUnspentPoints").innerHTML = "Unspent Points: " +user.unspent_points;
    userNode.querySelector(".InfoUnspentSkills").innerHTML = "Unspent Skills: " +user.unspent_skills;
    userNode.querySelector(".InfoWelcomeText").innerHTML = "Welcome Text: " +user.welcome_text;
    userNode.querySelector(".InfoHP").innerHTML = "HP: " +user.hp;
    userNode.querySelector(".InfoMP").innerHTML = "MP: " +user.mp;
    userNode.querySelector(".InfoDodgeChance").innerHTML = "Dodge Chance: " +user.dodge_chance;
    userNode.querySelector(".InfoDmgRed").innerHTML = "RedDamage: " +user.dmg_red;

    userInfoContainer.appendChild(userNode);
}