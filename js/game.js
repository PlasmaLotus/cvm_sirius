
let ctx = null;
let canevas = null;
let mousePosX = null;
let mousePosY = null;
let canAttack = true;
let playerStatusContainer = null;
let gameStatusContainer = null;
let playerSkillsContainer = null;
let gameEndStatus = null;
let audioHandler = new GameAudioHandler();
let STATUS_GAME_LOSS = 1;
let STATUS_GAME_WON = 2;
let STATUS_GAME_NOT_FOUND = 3;
//let actionsContainer = null;
let MINIMUM_TIME_REQUEST = 2001;
let gState = null;
let gStatePrev = null;
window.onload = () =>{
	canevas = document.getElementById("canvas");
	ctx = canevas.getContext("2d"); //graphics
	
	canevas.width = window.innerWidth;
    canevas.height = window.innerHeight;
	
	playerStatusContainer = document.getElementById("playerStatus");
	gameStatusContainer = document.getElementById("gameStatus");
	playerSkillsContainer = document.getElementById("actions");

	if (playerStatusContainer != null){
		playerStatusContainer.style.display = "flex";
		playerStatusContainer.style.position = "absolute";
		playerStatusContainer.style.left = "45%";
		playerStatusContainer.style.bottom = "0px";
	}
	if (gameStatusContainer != null){
		gameStatusContainer.style.display = "flex";
		gameStatusContainer.style.position = "absolute";
		gameStatusContainer.style.left = "45%";
		gameStatusContainer.style.top = "0px";
	}
	if (playerSkillsContainer != null){
		playerSkillsContainer.style.display = "flex";
		playerSkillsContainer.style.position = "absolute";
		playerSkillsContainer.style.left = "0px";
		playerSkillsContainer.style.top = "0px";
	}
	loadSoundButtons();

	setTimeout(updateGameStatus, MINIMUM_TIME_REQUEST);
	initAnimations();
	tick();
}

function tick(){
}

function updateGameStatus(){
	//console.log("Updating Game Status");
    $.ajax({
        type : "POST",
        url : "_infoRequest.php",
        data : {
            request: "gameState",
            sessionKey : "",
        }
    })
    .done(response => {
		response = JSON.parse(response);
        if (typeof response === "object") {
			_updateGameStatus(response);
			updatePlayerStatus(response);
			updatePlayerSkills(response);
			updateEvents(response);
			//test();
			setTimeout(updateGameStatus, MINIMUM_TIME_REQUEST);
		}
		else{
			console.log(response);
			if (response == "USER_NOT_FOUND" || response == "GAME_NOT_FOUND_NONE"){
				window.location.replace("lobby.php?gameNotFound=true");
			}
			if (response == "GAME_NOT_FOUND_LOST"){
				gameStatusContainer.innerHTML = "YOU HAVE LOST THE GAME...";
				setTimeout(backToLobbyLoss, 1500);
			}
			if (response == "GAME_NOT_FOUND_WIN"){
				gameStatusContainer.innerHTML = "YOU HAVE WON THE GAME!!!";
				setTimeout(backToLobbyWin, 1500);
			}
			if (response == "TOO_MANY_CALLS_BAN"){
				setTimeout(backToLobbyLoss, 500);
			}
		}
    });
}

function updatePlayerStatus(fullJSON){
	while(playerStatusContainer.firstChild){
		playerStatusContainer.removeChild(playerStatusContainer.firstChild);
	}
	let playerTemplate = document.querySelector("#playerInfoTemplate").innerHTML;
	let skillTemplate = document.querySelector("#SkillInfoTemplate").innerHTML;
	//For Current Player
	let player = fullJSON.player;
	//let node = document.createElement("div");
	let node = _updatePlayerStatus(player, playerTemplate, skillTemplate)
	playerStatusContainer.appendChild(node)
	
	//For other players
	for (let i = 0; i < fullJSON.other_players.length; i++) {
        //let node = document.createElement("div");
		let player = fullJSON.other_players[i];
		let node = _updatePlayerStatus(player, playerTemplate, skillTemplate)
		playerStatusContainer.appendChild(node);
        if (player.attacked){
			if (player.attacked != "--"){
				console.log(player.name + " attacked with " + player.attacked);
				if (audioHandler != null){
					audioHandler.queueEvent(player.attacked);
				}
				_animationAttack(player.attacked, player.name);
			}
		}
    }
}

function _updatePlayerStatus(player, playerTemplate, skillTemplate){
	let node = document.createElement("div");
	node.innerHTML = playerTemplate;
	node.querySelector(".InfoName").innerHTML = "Name: "+ player.name;
	node.querySelector(".InfoLevel").innerHTML = "Level: "+ player.level;
	node.querySelector(".InfoCurrentHP").innerHTML = "HP: "+ player.hp + "/" + player.max_hp;
    //node.querySelector(".InfoHP").innerHTML = "HP: "+ player.hp;
    //node.querySelector(".InfoMaxHP").innerHTML = "MaxHP: "+ player.max_hp;
	node.querySelector(".InfoCurrentMP").innerHTML = "MP: "+ player.mp + "/" + player.max_mp;
	//node.querySelector(".InfoMP").innerHTML = "MP: "+player.mp;
    //node.querySelector(".InfoMaxMP").innerHTML = "MaxMP: "+player.max_mp;
    node.querySelector(".InfoType").innerHTML = "Type: "+ player.type;
    node.querySelector(".InfoVictories").innerHTML = "Games Won: "+ player.victories;
	return node;
}

function _updateGameStatus(fullJSON){
	while(gameStatusContainer.firstChild){
		gameStatusContainer.removeChild(gameStatusContainer.firstChild);
	}
	let gameTemplate = document.querySelector("#GameInfoTemplate").innerHTML;
	let game = fullJSON.game;
	let gameNode = document.createElement("div");
	gameNode.innerHTML = gameTemplate;

	gameNode.querySelector(".GameInfoAttacked").innerHTML = "Attacked: " +game.attacked.toString();
	gameNode.querySelector(".GameInfoMaxHP").innerHTML = "HP: " + game.hp + "/" + game.max_hp;
	//gameNode.querySelector(".GameInfoHP").innerHTML = "HP: " +game.hp;
    //gameNode.querySelector(".GameInfoMaxHP").innerHTML = "MaxHP: " +game.max_hp;
    gameNode.querySelector(".GameInfoLastTarget").innerHTML = "LastTarget: " +game.last_target;
    gameNode.querySelector(".GameInfoLevel").innerHTML = "Level: " +game.level;
    gameNode.querySelector(".GameInfoName").innerHTML = "Name: " +game.name;
    gameNode.querySelector(".GameInfoType").innerHTML = "Type: " +game.type;
	gameStatusContainer.appendChild(gameNode);
}

function updatePlayerSkills(fullJSON){
	while(playerSkillsContainer.firstChild){
		playerSkillsContainer.removeChild(playerSkillsContainer.firstChild);
	}
	let skillTemplate = document.querySelector("#SkillInfoTemplate").innerHTML;

	if (fullJSON.player.skills){
		for (let i = 0; i < fullJSON.player.skills.length; i++) {
			let skill = fullJSON.player.skills[i];
			let node = _updatePlayerSkills(skill, skillTemplate)
			node.style.display="inline";
			node.onmousedown = function (event) {
				attack(fullJSON.player.skills[i].name);
			}
			/*
			node.onmouseenter = function (event) {
				console.log("Mouse");
				audioHandler.queueEvent("LobbyOver");
			}
			*/
			playerSkillsContainer.appendChild(node);
		}
	}
}

function _updatePlayerSkills(skill, skillTemplate){
	let skillNode = document.createElement("div");
	skillNode.innerHTML = skillTemplate;
	//node.style.display = "flex";
	//skillNode.querySelector(".SkillText").innerHTML = "This is a skill. Click it!";
	skillNode.querySelector(".SkillName").innerHTML = "Skill: "+ skill.name;
	skillNode.querySelector(".SkillCost").innerHTML = "Cost: "+ skill.cost;
	skillNode.querySelector(".SkillLevel").innerHTML = "Level: "+ skill.level;
	skillNode.querySelector(".SkillDamage").innerHTML = "Damage: "+ skill.dmg;
	skillNode.querySelector(".SkillHeal").innerHTML = "Heal: "+ skill.heal;
	if (skill.heal_target != null){
		skillNode.querySelector(".SkillHealTarget").innerHTML = "HealTarget: "+ skill.heal_target.toString();
	}
	//node.querySelector(".InfoSkills").appendChild(skillNode);
	return skillNode;
}


function attack(skillName){
	console.log("Attacking "+ skillName);
	if (canAttack){
		$.ajax({
			type : "POST",
			url : "_infoRequest.php",
			data : {
				request: "attack",
				attackName : skillName
			}
		})
		.done(response => {
			response = JSON.parse(response);
			if (response === "OK") {
				console.log("Attack OK");
				if (audioHandler != null){
					audioHandler.queueEvent(skillName);
				}
				_animationAttack(skillName, mainUsername);
			}
			else{
				console.log("Cannot Attack: "+ response);
			}
			setTimeout(_canAttack, MINIMUM_TIME_REQUEST);
		});
	}
	else{
		console.log("My Boi Chill!! You can't attack so soon (tm). Plz wait.");
	}
	canAttack = false;
}
function _canAttack(){
	canAttack = true;
}

function backToLobby(status){
	//console.log("redirecting");
	window.location.replace("lobby.php?game=false")
}
function backToLobbyWin(){
	backToLobby(STATUS_GAME_WON);
}
function backToLobbyLoss(){
	backToLobby(STATUS_GAME_LOSS);
}


function updateEvents(json){
	gState = json;
	let p = [gState.player, ...gState.other_players];
	if (gStatePrev != null && gState != null){
		let pPrev = [gStatePrev.player, ...gStatePrev.other_players];
		_updateEventsPlayers(p, pPrev);
		_updateEventsGame(gState, gStatePrev);

	}
	else{
		console.log("First Players Render Update");
		animationSetMainPlayerUsername(gState.player.name);
		_updateEventsPlayers(p, new Array());
		pointYou();
	}


	gStatePrev = gState;
}

function _updateEventsGame(frame, framePrev){
	
	//console.log(frame);
	if( !framePrev.game.attacked  && frame.game.attacked){
		//event Boss Attacked

		let target = frame.game.last_target;
		if(target){
			if (target == "party"){
				_animationGameAttackAll();
			}
			else{
				_animationGameAttackPlayer(target);
			}
		}
	}

	if (frame.game.hp >= 0){
		//Game Win
	}
}

function _updateEventsPlayers(players, playersPrev){

	//Find the Player from the current frame in the Previous Frame
	//But dont' add it to the list we're filthering
	//This should return the new player only 
	let newPlayers = players.filter(elem => {
		return undefined == playersPrev.find(function(e) {
				return e.name == elem.name;
			})
		}
	);
	newPlayers.forEach(function(elem){
		console.log(elem.name + " has joined the game!")
		audioHandler.queueEvent("PlayerJoin");
		_addAnimationPlayer(elem);
	});

	let oldPlayers = playersPrev.filter(elem => {
		return undefined == players.find(function(e) {
				return e.name == elem.name;
			})
		}
	);
	oldPlayers.forEach(function(elem){
		console.log(elem.name + " has left the game.");
		audioHandler.queueEvent("PlayerLeave");
		_removeAnimationPlayer(elem);

	});
	
}

/*Function to test a feature*/
function test(){
	if (gStatePrev != null && gState != null){

		let p = [gState.player, ...gState.other_players];
		let pPrev = [gStatePrev.player, ...gStatePrev.other_players];
		let t = pPrev.find(function(e) {
			//console.log(e);
			return e.name == "XDDX";
		});
		console.log(t);

	}
}