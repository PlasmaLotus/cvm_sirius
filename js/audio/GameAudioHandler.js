class GameAudioHandler{
    constructor(){
        this.sounds = new Array();
        this.events = new Array();
        this.seed = Math.floor((Math.random() * 10) + 1);
        let songs = ["audio/18 Invader GIRL! (Original Version).mp3", "audio/18 Invader GIRL! (Original Version).mp3"];
        this.music = new Audio("audio/Battle with the Boss.mp3");
        this.musicEnabled = true;
        this.soundEnabled = true;

        if (this.music != null){
            this.music.volume = 0.5;
            this.music.loop = true;
            this.music.play();
        }
    }
    tick(){
        for (let i = this.events.length-1 ; i >= 0; i--){
            let event = this.events[i];
            let sound = null;
            if(this.soundEnabled){
                sound = new Audio("audio/countdown.mp3");
                sound.volume = 0.5;
                if (event == "Normal"){
                    sound = new Audio("audio/song100.wav");
                }
                if (event == "Special1"){
                    sound = new Audio("audio/song103.wav");
                }
                if (event == "Special2"){
                    sound = new Audio("audio/song104.wav");
                }
                if (event == "BataAtk"){
                    sound = new Audio("audio/song161.wav");
                    //sound = new Audio("audio/song145.wav");
                }
                if (event == "BataAtk2"){
                    sound = new Audio("audio/song121.wav");
                }
                if (event == "BataSpe"){
                    
                }
                if (event == "BataSpe2"){
                    
                }
                if (event == "PlayerJoin"){
                    sound = new Audio("audio/song137.wav");
                }
                if (event == "PlayerLeave"){
                    sound = new Audio("audio/song110.wav");
                }
                
                if (sound != null){
                    sound.volume = 0.5;
                    sound.play();
                    this.sounds[this.sounds.length] = sound;
                    //console.log("soundPklayerd");
                }
            }
            this.events.splice(i, 1);
        }

        for (let i = this.sounds.length-1 ; i >= 0; i--){
            let sound = this.sounds[i];
            if (sound.ended){
                this.sounds.splice(i, 1);
            }
        }
    }

    disableMusic(){
        this.musicEnabled = false;
        if (this.music != null){
            this.music.muted = true;
        }
    }
    enableMusic(){
        this.musicEnabled = true;
        if (this.music != null){
            this.music.muted = false;
        }
    }
    disableSFX(){
        this.soundEnabled = false;
    }
    enableSFX(){
        this.musicEnabled = true;
    }
    queueEvent(event){
        //console.log("Queue event "+ event);
        this.events[this.events.length] = event;
    }

}