
let ctx = null;
let canevas = null;
let mousePosX = null;
let mousePosY = null;
let mainDiv = null;
let container = null;
let audioHandler = new LobbyAudioHandler();
let MINIMUM_TIME_REQUEST = 2001;
window.onload = () =>{
    
	canevas = document.getElementById("canvas");
	ctx = canevas.getContext("2d"); //graphics
    mainDiv = document.getElementById("container");
    musicButton = document.getElementById("musicTooLoud");
	sfxButton = document.getElementById("soundTooLoud");
	mainDiv.onmousemove = function (event) {
		mousePosX = event.offsetX;
		mousePosY = event.offsetY;
    }
    container = document.getElementById("gamesContainer")
    
    setTimeout(loadLobbyInfo, MINIMUM_TIME_REQUEST);
    loadSoundButtons();
	tick();
}

function tick(){
	ctx.clearRect(0,0,canevas.width, canevas.height);
    if (audioHandler != null){
		audioHandler.tick();
	}

	window.requestAnimationFrame(tick);
}

function joinGame(id){
    //console.log(game);
    console.log("Join game Attempt: "+ id);
    $.ajax({
        type : "POST",
        url : "_infoRequest.php",
        data : {
            request: "joinGame",
            gameid: id,
        }
    })
    .done(response => {
        response = JSON.parse(response);
        console.log(response);
        if(response === "GAME_ENTERED"){
            console.log(response);
            window.location.href = "game.php";
            //document.querySelector("#gameEntry").nodeValue = id;
        }
        else{
            console.log(response + " id: " + id);
        }
        
    });
}

function loadLobbyInfo(){
    $.ajax({
        type : "POST",
        url : "_infoRequest.php",
        data : {
            request: "lobby",
            sessionKey : "",
        }
    })
    .done(response => {
        while(container.firstChild){
            container.removeChild(container.firstChild);
        }
        response = JSON.parse(response);
        if (typeof response === "object") {
            //console.log(response);
            //console.log("this response is an object");
            let template = document.querySelector("#gameLobbyTemplateID").innerHTML;
            for (let i = 0; i < response.length; i++) {
                let node = document.createElement("div");
                let game = response[i];
                //id = game.id;
                node.innerHTML = template;
                node.id = "gameID"+game.id;
                node.querySelector(".gameInfoID").innerHTML = "GameID: "+ game.id;
                node.querySelector(".gameInfoName").innerHTML = "Name: "+  game.name;
                node.querySelector(".gameInfoLevel").innerHTML = "Level: "+  game.level;
                node.querySelector(".gameInfoHP").innerHTML = "HP: " + game.current_hp + "/" + game.hp;
                //node.querySelector(".gameInfoCurrentHP").innerHTML = "CurrentHP: "+ game.current_hp;
                //node.querySelector(".gameInfoHP").innerHTML = "HP: "+ game.hp;
                node.querySelector(".gameInfoUserCount").innerHTML = "PlayerCount: " + game.nb + "/" + game.max_users;
                //node.querySelector(".gameInfoNB").innerHTML = game.nb;
                //node.querySelector(".gameInfoMaxUsers").innerHTML = game.max_users;
                node.querySelector(".gameInfoType").innerHTML = "Type: " +game.type;
                node.querySelector(".gameInfoWinCount").innerHTML = "Wins: " +game.win_count;
                node.querySelector(".gameInfoLossCount").innerHTML = "Losses: " +game.loss_count;

                node.onmousedown = function(){joinGame(game.id)};
                container.appendChild(node)
            }
            setTimeout(loadLobbyInfo, MINIMUM_TIME_REQUEST);
        }
        else{
            console.log("Failed to load lobby...");
            container.innerHTML = response + " \nUnable to find games for you... Try Logging back in :$";
            if (response == "USER_NOT_FOUND"){
                window.location.replace("home.php");
            }
        }
        
    });
}